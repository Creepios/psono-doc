---
title: Configuration
metaTitle: Configuration of a Psono SaaS instance | Psono Documentation
meta:
  - name: description
    content: Instructions how to configure a Psono SaaS instance
---

# Configuration

## Preamble

We assume that you already have provisioned a Psono SaaS instance, if not please create one.

## Guide

1)  Go to "Psono SaaS" and click the pencil icon button

    ![Step 1 Go to "Psono SaaS" and click the "+"](/images/console/psono-saas-instance/creation-psono-saas-instance.png)

2)  Configure `settings.yaml`

    You can now modify all the settings according to your requirements. The settings look "cryptic" yet "map" to the
    settings of the regular settings of the regular `settings.yaml`. Details about each can be found in the regular
    Admin documentation here e.g. [/admin/overview/summary.html](/admin/overview/summary.html).
    
    ![Step 2 Modify settings.yaml](/images/console/psono-saas-instance/configuration-psono-saas-instance-settings.png)

3)  Complex parameters

    Some of the parameters like `OIDC_CONFIGURATIONS`, `SAML_CONFIGURATIONS`, `LDAPGATEWAY` and `LDAP` are more complex and
    as such need to be provided in json encoding. So you might be asked to configure something like this in your `settings.yaml`.
    
    ```yaml
    OIDC_CONFIGURATIONS:
        1:
            OIDC_RP_SIGN_ALGO: 'RS256'
            OIDC_RP_CLIENT_ID: 'xxxxxxxx.apps.googleusercontent.com'
            OIDC_RP_CLIENT_SECRET: 'xxxxxxx'
            OIDC_OP_JWKS_ENDPOINT: 'https://www.googleapis.com/oauth2/v3/certs'
            OIDC_OP_AUTHORIZATION_ENDPOINT: 'https://accounts.google.com/o/oauth2/v2/auth'
            OIDC_OP_TOKEN_ENDPOINT: 'https://oauth2.googleapis.com/token'
            OIDC_OP_USER_ENDPOINT: 'https://openidconnect.googleapis.com/v1/userinfo'
            OIDC_USERNAME_ATTRIBUTE: 'email'
    ```
    
    You'd first look for `OIDC_CONFIGURATIONS` in the console and then use a yaml to json converter like
    e.g. [onlineyamltools.com/convert-yaml-to-json](https://onlineyamltools.com/convert-yaml-to-json) to convert it.
    Afterwards you can just copy paste it as shown below:
    
    ![Step 4 Modify the config.json](/images/console/psono-saas-instance/configuration-psono-saas-instance-complex-paramters.png)
    

4)  Configure `config.json`

    Some of the documents require that you modify a `config.json`. There are two of those `config.json`, one for the regular
    webclient and one for the portal. You will find these settings at the end as shown in the screenshot below.
    
    ![Step 3 Modify the config.json](/images/console/psono-saas-instance/configuration-psono-saas-instance-config-json.png)



