---
title: V17 Mobile
metaTitle: V17 Mobile | Psono Documentation
meta:
  - name: description
    content: Mobile verification requirements
---

# V17 Mobile

Mobile verification requirements

[[toc]]

| ID   | Detailed Verification Requirement                                                                                                                                                                                      | Level 1                 | Level 2    | Level 3     | Since     |
|-------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---|---|---|-------|
| 17.1  | Verify that ID values stored on the device and retrievable by other applications, such as the UDID or IMEI number are not used as authentication tokens.                                                              | x | x | x | 2.0   |
| 17.2  | Verify that the mobile app does not store sensitive data onto potentially unencrypted shared resources on the device (e.g. SD card or shared folders).                                                                | x | x | x | 2.0   |
| 17.3  | Verify that sensitive data is not stored unprotected on the device, even in system protected areas such as key chains.                                                                                                | x | x | x | 2.0   |
| 17.4  | Verify that secret keys, API tokens, or passwords are dynamically generated in mobile applications.                                                                                                                   | x | x | x | 2.0   |
| 17.5  | Verify that the mobile app prevents leaking of sensitive information (for example, screenshots are saved of the current application as the application is backgrounded or writing sensitive information in console) . |   | x | x | 2.0   |
| 17.6  | Verify that the application is requesting minimal permissions for required functionality and resources.                                                                                                               |   | x | x | 2.0   |
| 17.7  | Verify that the application sensitive code is laid out unpredictably in memory (For example ASLR).                                                                                                                    | x | x | x | 2.0   |
| 17.8  | Verify that there are anti-debugging techniques present that are sufficient enough to deter or delay likely attackers from injecting debuggers into the mobile app (For example GDB).                                 |   |   | x | 2.0   |
| 17.9  | Verify that the app does not export sensitive activities, intents, content providers etc., for other mobile apps on the same device to exploit.                                                                       | x | x | x | 2.0   |
| 17.10 | Verify that sensitive information maintained in memory is overwritten with zeros as soon as it no longer required, to mitigate memory dumping attacks.                                                                |   | x | x | 3.0.1 |
| 17.11 | Verify that the app validates input to exported activities, intents, or content providers.                                                                                                                            | x | x | x | 3.0.1 |


### 17.1

Neither UDID nor IMEI are used as authentication.

### 17.2

All data stored is stored in secure storage. Psono App uses `flutter_secure_storage` module which uses:

- Keychain on iOS
- AES encryption for Android. The AES secret key is encrypted with RSA and the RSA key is stored in the KeyStore

### 17.3 (violation)

If keychains are considered insecure then you have no real alternative left. If one ignores the recommendation and trusts the key chains then one is save with Psono, as all content is stored encrypted.

### 17.4

Secret keys and API tokens are dynamically generated.

### 17.5

Psono is using `FLAG_SECURE` to prevent screenshots and the streaming to insecure screens.

### 17.6

Psono App only requests the absolute minimum set of permissions.

### 17.7 (violation)

I don't think that Flutter has an option for ASLR.

### 17.8 (violation)

I honestly don't know how to do that. Any help is appreciated.

### 17.9

The app does not export sensitive activities, intents, content providers etc. for other apps on the device.

### 17.10 (violation)

There is no mechanism implemented to do that.

### 17.11

The app validates input to exported activities, intents, or content providers.
