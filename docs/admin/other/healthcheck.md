---
title: Healthcheck
metaTitle: Healthcheck | Psono Documentation
meta:
  - name: description
    content: A brief description of the implemented healthcheck
---

# Healthcheck

To monitor the health of your own installation, Psono provides a health check resource.

```
GET /healthcheck/
```

It will (in case of a success) return a HTTP status code `200` and a `JSON` of the following format:

```json
{
    "time_sync":{
        "healthy":true
    },
    "db_read":{
        "healthy":true
    },
    "db_sync":{
        "healthy":true
    }
}
```

Any other HTTP status code is considered "unhealthy". The reason for an unhealthy state should be part of the json.

